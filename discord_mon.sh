#!/bin/bash

message="$1"
MYIP=$(ip addr show ens3 | grep "inet " | sed -e "s/.* inet //g" | sed -e "s/\/.*//g")

URL="https://discord.com/api/webhooks/1081555494809903165/C-EBdZb6IAASC20EiFQVhMgQLKQzzwEK-N30KKfL0khQDEHST05rIRnMVY59e42Ix_CY"

USERNAME="[$HOSTNAME/$MYIP]"

USERNAME=$( echo $USERNAME | sed -e "s/\/bin\/bash//" )

JSON="{\"username\": \"$USERNAME\", \"content\": \"$message\"}"

curl -s -X POST -H "Content-Type: application/json" -d "$JSON" $URL
