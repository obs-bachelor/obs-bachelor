# Ansible files for Moodle LMS

This changes the deployment method used previously in heat, use together with xxxAnsible.yaml HOT templates. It simplifies much of the setup and maintenance of the environment, not relying on bash scripts.

## Table of Contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
- [Roles](#roles)
- [Variables](#variables)

## Requirements

- Ansible core 2.14.5 or later
- Target systems should be running Ubuntu 22.04 or comparable (code is developed and made for Ubuntu, packages on other distros/package managers may vary)

## Installation

1. Clone this repository to your Ansible controller (might require SSH key access):
```
git clone git@gitlab.stud.idi.ntnu.no:obs-bachelor/obs-bachelor.git
```

2. Change to the ansible directory in the repository directory
```
cd obs-bachelor/ansible
```

3. Install required external Ansible roles (currently non):
```
ansible-galaxy install -r requirements.yml
```

## Usage

1. Configure your inventory file ('inventory.yaml') with your target hosts and desired groupings.

Example:
```
[database]
database01 ansible_host=192.168.0.2

[webserver]
webserver01 ansible_host=192.168.0.3

[proxy]
proxy01 ansible_host=192.168.0.4
```

2. Set your desired variables in the different 'vars' directories, such as 'roles/webserver/vars/main.yml' or 'group_vars/web_and_db.yaml'.

3. Run the ansible playbook:
```
ansible-playbook -i inventory.yaml playbook.yaml
```

## Roles

- `common`: Installs common packages and configures basic settings for all hosts.
- `database`: Sets up the MySQL server and configures the required databases and users.
- `webserver`: Installs and configures the web server (Apache), PHP, and Moodle.
- `proxy`: Sets up the reverse proxy and would handle SSL certificates.

## Variables

A list of important variables that can be configured for each role:

- `database`
- `db_root_pass`: MySQL root password
- `moodle_db_user`: Moodle database username
- `moodle_db_pass`: Moodle database password

- `webserver`
- `moodle_db_host`: Moodle database host IP
- `moodle_domain`: Moodle domain name (currently IP)
- `moodle_data_root`: Moodle data root directory
- `moodle_admin_password`: Moodle admin password
- `moodle_admin_email`: Moodle admin email
- `moodle_full_site_name`: Moodle full site name
- `moodle_short_site_name`: Moodle short site name