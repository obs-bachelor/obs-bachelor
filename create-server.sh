#!/bin/bash

DOMAIN=10.212.169.122
VMNAME=webtest
INSTALL1=moodle-webserver-setup.sh


openstack server create --image db1bc18e-81e3-477e-9067-eecaa459ec33 --flavor e2383865-a614-4dfe-97c6-b5d6e3369c7c --network 9dedf49d-abdb-4181-b668-9fc271a311ff --key-name manager --security-group HTTP --security-group default --user-data $INSTALL1 $VMNAME

sleep 30

openstack server add floating ip webtest $DOMAIN
