# Use an Ubuntu 20.04 base image
FROM ubuntu:20.04

# Set environment variables
ENV ENGINE nginx
ENV DOMAIN 10.212.169.122
ENV DBTYPE pgsql
ENV DBNAME moodle2
ENV DBUSER moodleuser
ENV DATAROOT /var/moodledata
ENV DBHOST 192.168.0.152
ENV DBPASS moodlepassword
ENV ADMINPASS P@ssword1
ENV ADMINEMAIL admin@mail.com
ENV FSITENAME Moodle-site
ENV SSITENAME moodle

# Set up a non-interactive frontend
ENV DEBIAN_FRONTEND noninteractive
RUN echo 'tzdata tzdata/Areas select Europe' | debconf-set-selections && \
    echo 'tzdata tzdata/Zones/Europe select Oslo' | debconf-set-selections

# Install required dependencies and update the system
RUN apt-get update && apt-get install -y \
    git \
    curl \
    wget \
    gnupg2 \
    software-properties-common \
    && rm -rf /var/lib/apt/lists/*

# Add the content of the script to the Dockerfile
COPY setup.sh /tmp/
RUN chmod +x /tmp/setup.sh && /tmp/setup.sh

# Expose Moodle on port 80
EXPOSE 80

# Set the entrypoint
ENTRYPOINT ["sh", "-c", "service nginx start && service php7.4-fpm start && tail -F /var/log/nginx/access.log /var/log/nginx/error.log"]
