heat_template_version: 2018-08-31
 
description: 
  # a description of the template
  >
  HOT template to setup reverse proxy using HAProxy

parameters:
  # declaration of input parameters
  manager_ssh_key:
    type: string
    description: Name of keypair used to access the compute instance(s)
    default: manager
  image:
    type: string
    description: ID or name of image used for the compute instance(s)
    default: Ubuntu Server 22.04 LTS (Jammy Jellyfish) amd64
  flavor:
    type: string
    description: ID or name of flavor used for the compute instance(s)
    default: gx1.2c4r
  public_net_id:
    type: string
    description: Name of the public network
    default: ntnu-internal
  mgmt_net:
    type: string
    description: Name of management network to be used
    default: moodle_mgmt_net
  mgmt_subnet:
    type: string
    description: Name of management subnet to use
    default: moodle_mgmt_subnet
  manager_ip:
    type: string
    description: IP address of the manager instance
    default: 192.168.0.50
  influx_ip:
    type: string
    description: Static IP for the influxDB instance
    default: 192.168.0.60
  static_ip:
    type: string
    description: Static IP for the compute instance
    default: 192.168.0.71

resources: 
  reverseProxy_sec:
    type: OS::Neutron::SecurityGroup
    properties:
      description: HTTP and HTTPS
      name: reverseProx
      rules:
        - remote_ip_prefix: 0.0.0.0/0
          protocol: tcp
          port_range_min: 80
          port_range_max: 80
        - remote_ip_prefix: 0.0.0.0/0
          protocol: tcp
          port_range_min: 443
          port_range_max: 443

  reverseproxy_port:
    type: OS::Neutron::Port
    properties:
      network_id: { get_param: mgmt_net }
      security_groups: 
        - default
        - { get_resource: reverseProxy_sec }
      fixed_ips:
        - subnet: { get_param: mgmt_subnet }
          ip_address: { get_param: static_ip }
  
  reverseproxy_public_ip:
    type: OS::Neutron::FloatingIP
    properties: 
      floating_network: { get_param: public_net_id }
      port_id:  { get_resource: reverseproxy_port }  

  reverseproxy:
    type: OS::Nova::Server
    properties:
      name: reverseproxy
      image:  { get_param: image }
      flavor: { get_param: flavor }
      key_name: { get_param: manager_ssh_key }
      networks:
        - port: { get_resource: reverseproxy_port }
      user_data: { get_file: lib/haproxy_reverse.sh }

outputs:
  floating_ip_output:
    description: Floating IP for reverseproxy
    value: { get_attr: [reverseproxy_public_ip, floating_ip_address] }
  