#!/bin/bash

## Variables

ENGINE=apache2 # nginx or apache2
DOMAIN=${PUBLIC_IP} #Set to 'public' ip
DBTYPE=mysqli #'pgsql', 'mariadb', 'mysqli', 'auroramysql', 'sqlsrv' or 'oci'
DBNAME=moodle
DBUSER=moodleuser
DATAROOT=/var/www/moodledata

DBHOST=192.168.0.91
DBPASS=MoodleP@ssword1

ADMINPASS=P@ssword1
ADMINEMAIL='admin@mail.com'
FSITENAME='Moodle-site'
SSITENAME='moodle'

MAININSTALL=webserver-1

NODE1=192.168.0.81
NODE2=192.168.0.82

## Functions

# Function to check if a node is online using ping
is_online() {
    local node=$1
    ping -c 1 -W 2 "$node" > /dev/null 2>&1
}

# Function to check if a node is connected in the GlusterFS cluster using gluster peer status
is_connected() {
    local node=$1
    gluster peer status | grep -q "Hostname: $node"
}


## Hosts file config

# Add manager to hosts file
echo "192.168.0.50 manager.lab manager" >> /etc/hosts
echo "192.168.0.60 influx.lab influx" >> /etc/hosts

# Set hostname
echo "$(hostname -I) $(hostname -s).lab $(hostname -s)" >> /etc/hosts
hostnamectl set-hostname $(hostname -s).lab

## Ubuntu Config ##

# Create a new ext4 file system on /dev/vdb1
mkfs.ext4 /dev/vdb

# Create the /storage directory if it doesn't exist
if [ ! -d /storage ]; then
  mkdir /storage
fi

# Mount the formatted volume to /storage
mount /dev/vdb /storage

# Add an entry to /etc/fstab to mount the volume at boot
echo '/dev/vdb /storage ext4 defaults 0 0' | sudo tee -a /etc/fstab

sed -i 's+//      "\${distro_id}:\${distro_codename}-updates";+        "\${distro_id}:\${distro_codename}-updates";+' /etc/apt/apt.conf.d/50unattended-upgrades  
systemctl restart unattended-upgrades 

## Installations ##

echo "Adding repositories, updating, installing and upgrading software..."
# influxdata-archive_compat.key GPG fingerprint:
#     9D53 9D90 D332 8DC7 D6C8 D3B9 D8FF 8E1F 7DF8 B07E
wget -q https://repos.influxdata.com/influxdata-archive_compat.key
echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null
echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list
add-apt-repository ppa:ondrej/php
add-apt-repository ppa:ondrej/apache2

apt-get update

apt-get -y install git curl telegraf glusterfs-server graphviz ghostscript clamav php7.4-pspell php7.4-curl php7.4-gd php7.4-intl php7.4-xml php7.4-xmlrpc php7.4-ldap php7.4-zip php7.4-soap php7.4-mbstring apache2 libapache2-mod-php7.4 aspell php7.4 php7.4-mysqli

#apt-get update  # might be unneeded 

#unattended-upgrade -d

## GlusterFS

systemctl enable --now glusterd

mkdir /storage/brick

if [ "$HOSTNAME" = $MAININSTALL.lab ]; then
  echo "On main install web server, setting up GlusterFS"
  gluster peer probe 192.168.0.82

  gluster volume create rep-volume replica 2 192.168.0.81:/storage/brick 192.168.0.82:/storage/brick 

  gluster volume start rep-volume
fi

while ! is_online "$NODE1" || ! is_online "$NODE2" || ! is_connected "$NODE1" || ! is_connected "$NODE2"; do
    echo "Waiting for nodes to be online and connected..."
    sleep 5
done

mkdir -p /var/www/moodledata

echo 'localhost:/rep-volume /var/www/moodledata glusterfs defaults,_netdev 0 0' | sudo tee -a /etc/fstab

mount -a

## PHP Config
echo "Changing PHP config"
sed -i "s:memory_limit = 128M:memory_limit = 256M:" /etc/php/7.4/apache2/php.ini
sed -i "s:;cgi.fix_pathinfo = 1:cgi.fix_pathinfo = 0:" /etc/php/7.4/apache2/php.ini
sed -i "s:upload_max_filesize = 2M:upload_max_filesize = 100M:" /etc/php/7.4/apache2/php.ini
sed -i "s:max_execution_time = 30:max_execution_time = 360:" /etc/php/7.4/apache2/php.ini
sed -i "s:;date.timezone =:date.timezone = Europe/Oslo:" /etc/php/7.4/apache2/php.ini

## Web server config

echo "Changing Apache2 DocumentRoot"
sed -i "s:DocumentRoot /var/www/html:DocumentRoot /var/www/html/moodle:" /etc/apache2/sites-available/000-default.conf 

service apache2 restart

if [ "$HOSTNAME" = "$MAININSTALL.lab" ]; then
## Moodle download and move ##
echo "Starting Moodle download"

cd /opt

git clone git://git.moodle.org/moodle.git

cd moodle

git branch --track MOODLE_401_STABLE origin/MOODLE_401_STABLE

git checkout MOODLE_401_STABLE

cp -R /opt/moodle /var/www/html/

## Moodle config ##

echo "Changing Moodle config"
sed -i "s:\$CFG->dbtype    = 'pgsql';:\$CFG->dbtype    = '${DBTYPE}';:" /var/www/html/moodle/config-dist.php 
sed -i "s:\$CFG->dbname    = 'moodle';:\$CFG->dbname    = '${DBNAME}';:" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dbhost    = 'localhost';:\$CFG->dbhost    = '${DBHOST}';:" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dbuser    = 'username';:\$CFG->dbuser    = '${DBUSER}';:" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dbpass    = 'password';:\$CFG->dbpass    = '${DBPASS}';:" /var/www/html/moodle/config-dist.php
sed -i "s+\$CFG->wwwroot   = 'http://example.com/moodle';+\$CFG->wwwroot   = 'http://${DOMAIN}';+" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dataroot  = '/home/example/moodledata';:\$CFG->dataroot  = '${DATAROOT}';:" /var/www/html/moodle/config-dist.php
sed -i "s://   \$CFG->tool_generator_users_password = 'examplepassword';:\$CFG->tool_generator_users_password = 'moodle';:" /var/www/html/moodle/config-dist.php #testing param

cp /var/www/html/moodle/config-dist.php /var/www/html/moodle/config.php 

chown -R www-data:www-data /var/www/moodledata
chown -R www-data:www-data /var/www/html/moodle 

## Moodle install ##

echo "Moodle installation starting"
/usr/bin/php /var/www/html/moodle/admin/cli/install_database.php --agree-license --adminpass=${ADMINPASS} --adminemail=${ADMINEMAIL} --fullname=${FSITENAME} --shortname=${SSITENAME}

scp -r /var/www/html/moodle ubuntu@$NODE2:/opt/moodle

touch /var/fileComplete.marker

scp /var/fileComplete.marker ubuntu@$NODE2:/opt/
fi

if [ "$HOSTNAME" == "webserver-2.lab" ];
  while [ ! -f "/opt/fileComplete.marker" ]; do
    echo "Waiting for folder transfer to complete..."
    sleep 5
  done
  cp -R /opt/moodle /var/www/html/

  chown -R www-data:www-data /var/moodledata
  chown -R www-data:www-data /var/www/html/moodle 
fi
## Add cron ##

# Add a cron job for the www-data user to run Moodle's cron script
(crontab -u www-data -l ; echo "*/1 * * * * php -q -f /var/www/html/moodle/admin/cli/cron.php") | crontab -u www-data -

## Reboot ##

#echo "Time to reboot"
#reboot