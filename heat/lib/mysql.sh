#!/bin/bash

## Variables
DBUSER=moodleuser
DBPASS=MoodleP@ssword1
DBROOT=SecureMoodleP@ssword1

## Hosts file config

# Add manager to hosts file
echo "192.168.0.50 manager.lab manager" >> /etc/hosts
echo "192.168.0.60 influx.lab influx" >> /etc/hosts

# Set hostname
echo "$(hostname -I) $(hostname -s).lab $(hostname -s)" >> /etc/hosts
hostnamectl set-hostname $(hostname -s).lab

## Ubuntu Config ##

sed -i 's+//      "\${distro_id}:\${distro_codename}-updates";+        "\${distro_id}:\${distro_codename}-updates";+' /etc/apt/apt.conf.d/50unattended-upgrades  
systemctl restart unattended-upgrades 

## Installations ##

echo "Adding repositories, updating, installing and upgrading software..."
# influxdata-archive_compat.key GPG fingerprint:
#     9D53 9D90 D332 8DC7 D6C8 D3B9 D8FF 8E1F 7DF8 B07E
wget -q https://repos.influxdata.com/influxdata-archive_compat.key
echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null
echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list

apt-get update

apt-get -y install telegraf mysql-server

#apt-get update  # might be unneeded 

#unattended-upgrade -d

## MySQL setup and config
echo "Configuring MySQL"

sed -i "s:bind-address		= 127.0.0.1:bind-address		= 0.0.0.0:" /etc/mysql/mysql.conf.d/mysqld.cnf

server-id               = 1                                                                                                             
log_bin                 = /var/log/mysql/mysql-bin.log                                                                                  
# binlog_expire_logs_seconds    = 2592000                                                                                              
max_binlog_size   = 100M                                                                                                                
binlog_do_db            = moodle  

systemctl restart mysql

mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '${DBROOT}';"
mysql -u root -p${DBROOT} -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH auth_socket;"

mysql -u root -p${DBROOT} -e "CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
mysql -u root -p${DBROOT} -e "CREATE user '${DBUSER}'@'%' IDENTIFIED BY '${DBPASS}';"
mysql -u root -p${DBROOT} -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO '${DBUSER}'@'%';"

root@database-1:/home/ubuntu# mysql -u root -p${DBROOT} -e "CREATE USER 'moodlereplicate'@'192.168.0.92' IDENTIFIED WITH mysql_native_pa│root@database-2:/home/ubuntu# nano /etc/mysql/mysql.conf.d/mysqld.cnf
ssword BY 'SuperSecureMoodleReplicate';"     
root@database-1:/home/ubuntu# mysql -u root -p${DBROOT} -e "GRANT REPLICATION SLAVE ON *.* TO 'moodlereplicate'@'192.168.0.92';" 