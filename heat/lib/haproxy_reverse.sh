#!/bin/bash

## Variables

DOMAIN=${PUBLIC_IP} #Set to 'public' ip
WEBSERVER1=192.168.0.80
HASTATUSER=haAdmin
HASTATPASS=SuperSecureMoodleHaAdminPassword

## Hosts file config

# Add manager to hosts file
echo "192.168.0.50 manager.lab manager" >> /etc/hosts
echo "192.168.0.60 influx.lab influx" >> /etc/hosts

# Set hostname
echo "$(hostname -I) $(hostname -s).lab $(hostname -s)" >> /etc/hosts
hostnamectl set-hostname $(hostname -s).lab

## Ubuntu Config ##

sed -i 's+//      "\${distro_id}:\${distro_codename}-updates";+        "\${distro_id}:\${distro_codename}-updates";+' /etc/apt/apt.conf.d/50unattended-upgrades  
systemctl restart unattended-upgrades 

## Installations ##

echo "Adding repositories, updating, installing and upgrading software..."
# influxdata-archive_compat.key GPG fingerprint:
#     9D53 9D90 D332 8DC7 D6C8 D3B9 D8FF 8E1F 7DF8 B07E
wget -q https://repos.influxdata.com/influxdata-archive_compat.key
echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null
echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list

apt-get update

apt-get -y install haproxy telegraf net-tools

## Configure and setup HAProxy

sed -i "s:ENABLED=0:ENABLED=1:" /etc/default/haproxy

systemctl enable haproxy

mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg.old

cat << EOF > /etc/haproxy/haproxy.cfg
global
  log 127.0.0.1 local0 notice
  maxconn 2000
  user haproxy
  group haproxy

defaults
  log global
  mode http
  option httplog
  option dontlognull
  retries 3
  option redispatch
  timeout connect 300000
  timeout client 300000
  timeout server 300000

frontend proxy
  bind *:80

  #ACL function declarations
  #acl is_abuse src_http_req_rate(Abuse) ge 10
  #acl inc_abuse_cnt src_inc_gpc0(Abuse) gt 0
  #acl abuse_cnt src_get_gpc0(Abuse) gt 0

  # Rules
  #tcp-request connection track-sc0 src table Abuse
  #tcp-request connection reject if abuse_cnt
  #http-request deny if abuse_cnt
  #http-request deny if is_abuse inc_abuse_cnt

  option httpclose
  option forwardfor

  use_backend moodle

backend moodle
  server web 192.168.0.80:80 check

#backend Abuse
  #stick-table type ip size 100k expire 30m store gpc0,http_req_rate(10s)

listen stats
  bind *:8080
  mode http
  stats enable
  stats hide-version
  stats uri /haproxy?stats
  stats realm HAProxy\ Statistics
EOF

systemctl restart haproxy

while true; do
  if ! ss -tuln | grep -q ':80\s'; then
    echo "HAProxy is not listening on port 80. Attempting to start the service..."
    systemctl restart haproxy
    sleep 5
  else
    echo "HAProxy is listening on port 80. Exiting..."
    break
  fi
done