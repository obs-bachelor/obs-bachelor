#!/bin/bash

## Variables

INFLUX_BUCK=moodle
INFLUX_ORG=obs-group
INFLUX_USER=admin
INFLUX_PASS=P@ssword1

## Hosts config
# Add manager to hosts file
echo "192.168.0.50 manager.lab manager" >> /etc/hosts

# Set hostname
echo "$(hostname -I) $(hostname -s).lab $(hostname -s)" >> /etc/hosts
hostnamectl set-hostname $(hostname -s).lab

## Ubuntu Config ##

sed -i 's+//      "\${distro_id}:\${distro_codename}-updates";+        "\${distro_id}:\${distro_codename}-updates";+' /etc/apt/apt.conf.d/50unattended-upgrades  
systemctl restart unattended-upgrades 

## Installations ##

echo "Adding repositories, updating, installing and upgrading software..."
# influxdata-archive_compat.key GPG fingerprint:
#     9D53 9D90 D332 8DC7 D6C8 D3B9 D8FF 8E1F 7DF8 B07E
wget -q https://repos.influxdata.com/influxdata-archive_compat.key
echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null
echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list

apt-get update

apt-get -y install telegraf influxdb2 influxdb2-cli

# Package upgrades
unattended-upgrades -d

apt-get update # second to double on available packages

unattended-upgrade -d # Try to catch missed packages 

## Start and enable InfluxDB

service influxdb start
service influxdb enable

## configure influx
influx setup \
  --org ${INFLUX_ORG} \
  --bucket ${INFLUX_BUCK} \
  --username ${INFLUX_USER} \
  --password ${INFLUX_PASS} \
  --force 

## Reboot server
reboot