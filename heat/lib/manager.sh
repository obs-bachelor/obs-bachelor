#!/bin/bash

## Variables

USERNAME=tempuser
PASSWORD=replaceme

## Hosts config

# Set hostname
echo "$(hostname -I) $(hostname -s).lab $(hostname -s)" >> /etc/hosts
hostnamectl set-hostname $(hostname -s).lab

# Add influx to hosts
echo "192.168.0.60 influx.lab influx" >> /etc/hosts

## Ubuntu Config ##

sed -i 's+//      "\${distro_id}:\${distro_codename}-updates";+        "\${distro_id}:\${distro_codename}-updates";+' /etc/apt/apt.conf.d/50unattended-upgrades  
systemctl restart unattended-upgrades 

## Installations and updates

# Update apt
apt-get update

# Install pip3, openstackclient and heatclient
apt-get -y install python3-pip

pip install python-openstackclient python-heatclient

# Upgrade system

unattended-upgrade -d

apt-get update # second to double on available packages

unattended-upgrade -d # Try to catch missed packages 

## Add OpenRC file - Need 

cat << 'EOF' > /home/ubuntu/DCST2900_V23_Orange-openrc.sh
#!/usr/bin/env bash
# To use an OpenStack cloud you need to authenticate against the Identity
# service named keystone, which returns a **Token** and **Service Catalog**.
# The catalog contains the endpoints for all services the user/tenant has
# access to - such as Compute, Image Service, Identity, Object Storage, Block
# Storage, and Networking (code-named nova, glance, keystone, swift,
# cinder, and neutron).
#
# *NOTE*: Using the 3 *Identity API* does not necessarily mean any other
# OpenStack API is version 3. For example, your cloud provider may implement
# Image API v1.1, Block Storage API v2, and Compute API v2.0. OS_AUTH_URL is
# only for the Identity API served through keystone.
export OS_AUTH_URL=https://api.skyhigh.iik.ntnu.no:5000
# With the addition of Keystone we have standardized on the term **project**
# as the entity that owns the resources.
export OS_PROJECT_ID=d9f6f436982c48c190e4fb8d2fc926d6
export OS_PROJECT_NAME="DCST2900_V23_Orange"
export OS_USER_DOMAIN_NAME="NTNU"
if [ -z "$OS_USER_DOMAIN_NAME" ]; then unset OS_USER_DOMAIN_NAME; fi
export OS_PROJECT_DOMAIN_ID="cb782810849b4ce8bce7f078cc193b19"
if [ -z "$OS_PROJECT_DOMAIN_ID" ]; then unset OS_PROJECT_DOMAIN_ID; fi
# unset v2.0 items in case set
unset OS_TENANT_ID
unset OS_TENANT_NAME
# In addition to the owning entity (tenant), OpenStack stores the entity
# performing the action as the **user**.
export OS_USERNAME="USER_PLACEMENT"
# With Keystone you pass the keystone password.
#echo "Please enter your OpenStack Password for project $OS_PROJECT_NAME as user $OS_USERNAME: "
#read -sr OS_PASSWORD_INPUT
export OS_PASSWORD="PASSWORD_PLACEMENT"
# Check to see if username and password has been set/changed
if [ "$OS_USERNAME" == "USER_PLACEMENT ] || [ "$OS_PASSWORD" == "PASSWORD_PLACEMENT" ]; then
echo "You forgot to change the username or password"
fi 
# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
export OS_REGION_NAME="SkyHiGh"
# Don't leave a blank variable, unset it if it was empty
if [ -z "$OS_REGION_NAME" ]; then unset OS_REGION_NAME; fi
export OS_INTERFACE=public
export OS_IDENTITY_API_VERSION=3
EOF

# Replace username and password in openrc file

sed -i "s:USER_PLACEMENT:$USERNAME:" /home/ubuntu/DCST2900_V23_Orange-openrc.sh
sed -i "s:PASSWORD_PLACEMENT:$PASSWORD:" /home/ubuntu/DCST2900_V23_Orange-openrc.sh

## Reboot
reboot
