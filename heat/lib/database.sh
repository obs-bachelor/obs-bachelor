#!/bin/bash

## Variables
DBUSER=moodleuser
DBPASS=MoodleP@ssword1
DBROOT=SuperSecureMoodlePassword

MODE=${MODE_IM}

## Hosts file config

# Add manager to hosts file
echo "192.168.0.50 manager.lab manager" >> /etc/hosts
echo "192.168.0.60 influx.lab influx" >> /etc/hosts

# Set hostname
echo "$(hostname -I) $(hostname -s).lab $(hostname -s)" >> /etc/hosts
hostnamectl set-hostname $(hostname -s).lab

## Ubuntu Config ##

sed -i 's+//      "\${distro_id}:\${distro_codename}-updates";+        "\${distro_id}:\${distro_codename}-updates";+' /etc/apt/apt.conf.d/50unattended-upgrades  
systemctl restart unattended-upgrades 

## Installations ##

echo "Adding repositories, updating, installing and upgrading software..."
# influxdata-archive_compat.key GPG fingerprint:
#     9D53 9D90 D332 8DC7 D6C8 D3B9 D8FF 8E1F 7DF8 B07E
wget -q https://repos.influxdata.com/influxdata-archive_compat.key
echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null
echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list

apt-get update

apt-get -y install telegraf

if [ $MODE == "mysql" ]; then
  echo "Setting mode to mysql"
  apt-get -y install mysql-server

  ## MySQL setup and config
  echo "Configuring MySQL"

  sed -i "s:bind-address		= 127.0.0.1:bind-address		= 0.0.0.0:" /etc/mysql/mysql.conf.d/mysqld.cnf

  systemctl restart mysql

  mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '${DBROOT}';"  
  mysql -u root -p${DBROOT} -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH auth_socket;"

  mysql -u root -p${DBROOT} -e "CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
  mysql -u root -p${DBROOT} -e "CREATE user '${DBUSER}'@'%' IDENTIFIED BY '${DBPASS}';"
  mysql -u root -p${DBROOT} -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO '${DBUSER}'@'%';"

elif [ $MODE == "postgresql" ]; then
  echo "Setting mode to PostgreSQL"
  apt-get -y install postgresql postgresql-contrib

  systemctl enable postgresql

  echo "Configuring PostgreSQL"
  sed -i '/host    all             all             127.0.0.1\/32            scram-sha-256/a host    all             all             192.168.0.0/24          scram-sha-256' /etc/postgresql/14/main/pg_hba.conf
  sed -i "s:#listen_addresses = 'localhost':listen_addresses = '\*':" /etc/postgresql/14/main/postgresql.conf
  systemctl restart postgresql

  sudo -u postgres psql -c "CREATE USER ${DBUSER} WITH PASSWORD '${DBPASS}';"
  sudo -u postgres psql -c "CREATE DATABASE moodle WITH OWNER ${DBUSER};"
  
elif [ $MODE == "mariadb" ]; then
  echo "Setting mode to MariaDB"
  apt-get -y install mariadb-server

  echo "Configuring MariaDB"

  # DEBUG
  echo "Before sed:"
  cat /etc/mysql/mariadb.conf.d/50-server.cnf | grep bind-address

  sed -i "s:bind-address            = 127.0.0.1:#bind-address            = 127.0.0.1:" /etc/mysql/mariadb.conf.d/50-server.cnf

  echo "After sed:"
  cat /etc/mysql/mariadb.conf.d/50-server.cnf | grep bind-address

  #DEBUG END

  systemctl restart mysqld

  mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '${DBROOT}' PASSWORD EXPIRE NEVER;"

  mysql -u root -p${DBROOT} -e "CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
  mysql -u root -p${DBROOT} -e "CREATE user '${DBUSER}'@'%' IDENTIFIED BY '${DBPASS}';"
  mysql -u root -p${DBROOT} -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO '${DBUSER}'@'%';"

fi

#apt-get update  # might be unneeded 

#unattended-upgrade -d
