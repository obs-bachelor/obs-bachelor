#!/bin/bash

## Variables

ENGINE=apache2 # nginx or apache2
DOMAIN=${PUBLIC_IP} #Set to 'public' ip
DBTYPE=mysqli #'pgsql', 'mariadb', 'mysqli', 'auroramysql', 'sqlsrv' or 'oci'
DBNAME=moodle
DBUSER=moodleuser
DATAROOT=/var/moodledata

DBHOST=127.0.0.1
DBPASS=MoodleP@ssword1

DBROOT=MoodleSuperSecure

ADMINPASS=P@ssword1
ADMINEMAIL='admin@mail.com'
FSITENAME='Moodle-site'
SSITENAME='moodle'

## Hosts file config

# Add manager to hosts file
echo "192.168.0.50 manager.lab manager" >> /etc/hosts
echo "192.168.0.60 influx.lab influx" >> /etc/hosts

# Set hostname
echo "$(hostname -I) $(hostname -s).lab $(hostname -s)" >> /etc/hosts
hostnamectl set-hostname $(hostname -s).lab

## Ubuntu Config ##

sed -i 's+//      "\${distro_id}:\${distro_codename}-updates";+        "\${distro_id}:\${distro_codename}-updates";+' /etc/apt/apt.conf.d/50unattended-upgrades  
systemctl restart unattended-upgrades 

## Installations ##

echo "Adding repositories, updating, installing and upgrading software..."
# influxdata-archive_compat.key GPG fingerprint:
#     9D53 9D90 D332 8DC7 D6C8 D3B9 D8FF 8E1F 7DF8 B07E
wget -q https://repos.influxdata.com/influxdata-archive_compat.key
echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null
echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list
add-apt-repository ppa:ondrej/php
add-apt-repository ppa:ondrej/apache2

apt-get update

apt-get -y install git curl telegraf graphviz ghostscript clamav php7.4-pspell php7.4-curl php7.4-gd php7.4-intl php7.4-xml php7.4-xmlrpc php7.4-ldap php7.4-zip php7.4-soap php7.4-mbstring apache2 libapache2-mod-php7.4 aspell php7.4 php7.4-mysqli mysql-server

#apt-get update  # might be unneeded 

#unattended-upgrade -d

## PHP Config
echo "Changing PHP config"
sed -i "s:memory_limit = 128M:memory_limit = 256M:" /etc/php/7.4/apache2/php.ini
sed -i "s:;cgi.fix_pathinfo = 1:cgi.fix_pathinfo = 0:" /etc/php/7.4/apache2/php.ini
sed -i "s:upload_max_filesize = 2M:upload_max_filesize = 100M:" /etc/php/7.4/apache2/php.ini
sed -i "s:max_execution_time = 30:max_execution_time = 360:" /etc/php/7.4/apache2/php.ini
sed -i "s:;date.timezone =:date.timezone = Europe/Oslo:" /etc/php/7.4/apache2/php.ini

## MySQL setup and config
echo "Configuring MySQL"

mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '${DBROOT}';"
mysql -u root -p${DBROOT} -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH auth_socket;"

mysql -u root -p${DBROOT} -e "CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
mysql -u root -p${DBROOT} -e "CREATE user '${DBUSER}'@'localhost' IDENTIFIED BY '${DBPASS}';"
mysql -u root -p${DBROOT} -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO '${DBUSER}'@'localhost';"

## Moodle download and move ##
echo "Starting Moodle download"

cd /opt

git clone git://git.moodle.org/moodle.git

cd moodle

git branch --track MOODLE_401_STABLE origin/MOODLE_401_STABLE

git checkout MOODLE_401_STABLE

cp -R /opt/moodle /var/www/html/

mkdir /var/moodledata

## Moodle config ##

echo "Changing Moodle config"
sed -i "s:\$CFG->dbtype    = 'pgsql';:\$CFG->dbtype    = '${DBTYPE}';:" /var/www/html/moodle/config-dist.php 
sed -i "s:\$CFG->dbname    = 'moodle';:\$CFG->dbname    = '${DBNAME}';:" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dbuser    = 'username';:\$CFG->dbuser    = '${DBUSER}';:" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dbpass    = 'password';:\$CFG->dbpass    = '${DBPASS}';:" /var/www/html/moodle/config-dist.php
sed -i "s+\$CFG->wwwroot   = 'http://example.com/moodle';+\$CFG->wwwroot   = 'http://${DOMAIN}';+" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dataroot  = '/home/example/moodledata';:\$CFG->dataroot  = '${DATAROOT}';:" /var/www/html/moodle/config-dist.php
sed -i "s://   \$CFG->tool_generator_users_password = 'examplepassword';:\$CFG->tool_generator_users_password = 'moodle';:" /var/www/html/moodle/config-dist.php #testing param

cp /var/www/html/moodle/config-dist.php /var/www/html/moodle/config.php 

chown -R www-data:www-data /var/moodledata
chown -R www-data:www-data /var/www/html/moodle 

## Web server config

echo "Changing Apache2 DocumentRoot"
sed -i "s:DocumentRoot /var/www/html:DocumentRoot /var/www/html/moodle:" /etc/apache2/sites-available/000-default.conf 
sed -i "s:#Require ip 192.0.2.0/24:Require ip 192.168.0.0/24:" /etc/apache2/mods-enabled/status.conf 
#Require ip 192.0.2.0/24

service apache2 restart

## Moodle install ##

echo "Moodle installation starting"
/usr/bin/php /var/www/html/moodle/admin/cli/install_database.php --agree-license --adminpass=${ADMINPASS} --adminemail=${ADMINEMAIL} --fullname=${FSITENAME} --shortname=${SSITENAME}

## Add cron ##

# Add a cron job for the www-data user to run Moodle's cron script
(crontab -u www-data -l ; echo "*/1 * * * * php -q -f /var/www/html/moodle/admin/cli/cron.php") | crontab -u www-data -

## Reboot ##

#echo "Time to reboot"
#reboot