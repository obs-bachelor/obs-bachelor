#!/bin/bash

ENGINE=${ENGINE_IM} # nginx or apache2
DOMAIN=${PUBLIC_IP} #Set to 'public' ip
MODE=${MODE_IM}
DBNAME=moodle
DBUSER=moodleuser
DBPASS=MoodleP@ssword1
DATAROOT=/var/moodledata
DBHOST=192.168.0.90

if [ $MODE == 'postgresql' ]; then
    DBTYPE=pgsql #'pgsql', 'mariadb', 'mysqli', 'auroramysql', 'sqlsrv' or 'oci'
    echo "PostgreSQL Mode selected"
elif [ $MODE == 'mysql' ]; then
    DBTYPE=mysqli #'pgsql', 'mariadb', 'mysqli', 'auroramysql', 'sqlsrv' or 'oci'
    echo "MySQL Mode selected"
elif [ $MODE == 'mariadb' ]; then
    DBTYPE=mariadb #'pgsql', 'mariadb', 'mysqli', 'auroramysql', 'sqlsrv' or 'oci'
    echo "MariaDB Mode selected"
fi

ADMINPASS=P@ssword1
ADMINEMAIL='admin@mail.com'
FSITENAME='Moodle-site'
SSITENAME='moodle'

## Ubuntu Config ##

sed -i 's+//      "\${distro_id}:\${distro_codename}-updates";+        "\${distro_id}:\${distro_codename}-updates";+' /etc/apt/apt.conf.d/50unattended-upgrades  
systemctl restart unattended-upgrades 

## Installations ##

echo "Adding repositories, updating, installing and upgrading software..."
# influxdata-archive_compat.key GPG fingerprint:
#     9D53 9D90 D332 8DC7 D6C8 D3B9 D8FF 8E1F 7DF8 B07E
wget -q https://repos.influxdata.com/influxdata-archive_compat.key
echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null
echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list
add-apt-repository ppa:ondrej/php

if [ $ENGINE == 'nginx' ]; then
    add-apt-repository ppa:ondrej/nginx 
elif [ $ENGINE == 'apache2' ]; then
    add-apt-repository ppa:ondrej/apache2
fi

apt-get update
apt-get -y install telegraf git curl graphviz ghostscript clamav php7.4-pspell php7.4-curl php7.4-gd php7.4-intl php7.4-xml php7.4-xmlrpc php7.4-ldap php7.4-zip php7.4-soap php7.4-mbstring 

if [ $ENGINE == 'nginx' ]; then
    apt-get -y install nginx  php7.4-fpm

    echo "Changing PHP config"
    sed -i "s:memory_limit = 128M:memory_limit = 256M:" /etc/php/7.4/fpm/php.ini
    sed -i "s:;cgi.fix_pathinfo = 1:cgi.fix_pathinfo = 0:" /etc/php/7.4/fpm/php.ini
    sed -i "s:upload_max_filesize = 2M:upload_max_filesize = 100M:" /etc/php/7.4/fpm/php.ini
    sed -i "s:max_execution_time = 30:max_execution_time = 360:" /etc/php/7.4/fpm/php.ini
    sed -i "s:;date.timezone =:date.timezone = Europe/Oslo:" /etc/php/7.4/fpm/php.ini
elif [ $ENGINE == 'apache2' ]; then
    apt-get -y install apache2 libapache2-mod-php7.4 aspell php7.4

    echo "Changing PHP config"
    sed -i "s:memory_limit = 128M:memory_limit = 256M:" /etc/php/7.4/apache2/php.ini
    sed -i "s:;cgi.fix_pathinfo = 1:cgi.fix_pathinfo = 0:" /etc/php/7.4/apache2/php.ini
    sed -i "s:upload_max_filesize = 2M:upload_max_filesize = 100M:" /etc/php/7.4/apache2/php.ini
    sed -i "s:max_execution_time = 30:max_execution_time = 360:" /etc/php/7.4/apache2/php.ini
    sed -i "s:;date.timezone =:date.timezone = Europe/Oslo:" /etc/php/7.4/apache2/php.ini
fi

if [ $DBTYPE == 'pgsql' ]; then
    apt-get -y install php7.4-pgsql
elif [ $DBTYPE == 'mysqli' ]; then
    apt-get -y install php7.4-mysqli
elif [ $DBTYPE == 'mariadb' ]; then
    apt-get -y install php7.4-mysqli
fi

#apt-get update  # might be unneeded 

#unattended-upgrade -d

## Moodle download and move ##
echo "Starting Moodle download"

cd /opt

git clone git://git.moodle.org/moodle.git

cd moodle

git branch --track MOODLE_401_STABLE origin/MOODLE_401_STABLE

git checkout MOODLE_401_STABLE

cp -R /opt/moodle /var/www/html/

mkdir /var/moodledata

## Moodle config ##

echo "Changing Moodle config"
sed -i "s:\$CFG->dbtype    = 'pgsql';:\$CFG->dbtype    = '${DBTYPE}';:" /var/www/html/moodle/config-dist.php 
sed -i "s:\$CFG->dbhost    = 'localhost';:\$CFG->dbhost    = '${DBHOST}';:" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dbname    = 'moodle';:\$CFG->dbname    = '${DBNAME}';:" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dbuser    = 'username';:\$CFG->dbuser    = '${DBUSER}';:" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dbpass    = 'password';:\$CFG->dbpass    = '${DBPASS}';:" /var/www/html/moodle/config-dist.php
sed -i "s+\$CFG->wwwroot   = 'http://example.com/moodle';+\$CFG->wwwroot   = 'http://${DOMAIN}';+" /var/www/html/moodle/config-dist.php
sed -i "s:\$CFG->dataroot  = '/home/example/moodledata';:\$CFG->dataroot  = '${DATAROOT}';:" /var/www/html/moodle/config-dist.php
sed -i "s://   \$CFG->tool_generator_users_password = 'examplepassword';:\$CFG->tool_generator_users_password = 'moodle';:" /var/www/html/moodle/config-dist.php #testing param

cp /var/www/html/moodle/config-dist.php /var/www/html/moodle/config.php 

chown -R www-data:www-data /var/moodledata
chown -R www-data:www-data /var/www/html/moodle 

## Engine config 

if [ $ENGINE == 'nginx' ]; then
    echo "Changing Nginx config - adding moodle site config"
    cat <<EOF > /etc/nginx/sites-available/moodle.conf
    server {
        listen   80;
        server_name $DOMAIN;
        root  /var/www/html/moodle;
        index index.php index.html index.htm;
    
        location ~ [^/].php(/|$) {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/run/php/php7.4-fpm.sock;
            fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
            include fastcgi_params;
        }
        location /nginx_status {
            stub_status;
            access_log off;
        }
    }
EOF

    ln -s /etc/nginx/sites-available/moodle.conf /etc/nginx/sites-enabled/moodle.conf

    service nginx restart
elif [ $ENGINE = 'apache2' ]; then
    echo "Changing Apache2 DocumentRoot"
    sed -i "s:DocumentRoot /var/www/html:DocumentRoot /var/www/html/moodle:" /etc/apache2/sites-available/000-default.conf 
    sed -i "s:#Require ip 192.0.2.0/24:Require ip 192.168.0.0/24:" /etc/apache2/mods-enabled/status.conf 
    #Require ip 192.0.2.0/24

    service apache2 restart
fi

## Moodle install ##

echo "Moodle installation starting"
/usr/bin/php /var/www/html/moodle/admin/cli/install_database.php --agree-license --adminpass=${ADMINPASS} --adminemail=${ADMINEMAIL} --fullname=${FSITENAME} --shortname=${SSITENAME}

## Add cron ##

# Add a cron job for the www-data user to run Moodle's cron script
(crontab -u www-data -l ; echo "*/1 * * * * php -q -f /var/www/html/moodle/admin/cli/cron.php") | crontab -u www-data -

## Reboot ##

#echo "Time to reboot"
#reboot
