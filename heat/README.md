# HEAT Orchestraction Templates (HOT)


## What is this?
This is a collection of the different stacks that will be used to test/run Moodle. 
More specifically;
  manager.yaml is HOT for the manager/bastion server (currently set to run on a 2c2r flavour)
  influx.yaml is HOT for the InfluxDB server
  monolithic.yaml is HOT for a full LAMP stack on one compute instance

## How to use this
First ensure that you have openstackcli and heatcli installed, this can be done by running following pip commands:
```
pip install python-openstackclient python-heatclient
```

Then to you want to ensure that you have sourced an OpenRC file
Once that is done, you want to setup manager first (this includes a OpenRC file for SkyHiGh use)
```
openstack stack create -t manager.yaml managerStack
```

Here you want to ssh into the manager, generate an SSH key and upload it as a keypair to OpenStack with the name manager. If you have openrc file on manager and source this you can upload the keypair for manager
```
ssh ubuntu@[manager.ip]
ssh-keygen
openstack keypair create --public-key .ssh/id_rsa.pub manager
```

Next you want the Influx server up and running, so repeat the above but for influx
```
openstack stack create -t influx.yaml influxStack
```

Finally you can setup which ever implementation you want to run by running
```
openstack stack create -t [implementation].yaml [stackName]
```